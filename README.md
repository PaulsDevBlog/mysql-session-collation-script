MySQL Session Collation Script
====================

Handy script to use when you start up a new MySQL Workbench or client session.

This repo contains MySQL session collation script discussed within the respective 
PaulsDevBlog.com article: https://www.paulsdevblog.com/mysql-quick-session-collation-script/

For other articles and code, please visit:
https://www.paulsdevblog.com
